# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/erikson/cs236/r-tree/src/dbcreate.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/dbcreate.dir/src/dbcreate.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "PF_LOG"
  "PF_STATS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/DependInfo.cmake"
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/rm.dir/DependInfo.cmake"
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/sm.dir/DependInfo.cmake"
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/ql.dir/DependInfo.cmake"
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/ix.dir/DependInfo.cmake"
  "/home/erikson/cs236/r-tree/build_log/CMakeFiles/parser.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
