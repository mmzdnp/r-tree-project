# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/erikson/cs236/r-tree/src/pf_buffermgr.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_buffermgr.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_error.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_error.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_filehandle.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_filehandle.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_hashtable.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_hashtable.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_manager.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_manager.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_pagehandle.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_pagehandle.cc.o"
  "/home/erikson/cs236/r-tree/src/pf_statistics.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/pf_statistics.cc.o"
  "/home/erikson/cs236/r-tree/src/statistics.cc" "/home/erikson/cs236/r-tree/build_log/CMakeFiles/pf.dir/src/statistics.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "PF_LOG"
  "PF_STATS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
