//
// File:        ix_indexmanager.cc
// Description: IX_IndexHandle handles indexes
// Author:      Yifei Huang (yifei@stanford.edu)
//

#include <unistd.h>
#include <sys/types.h>
#include "ix.h"
#include "pf.h"
#include <climits>
#include <string>
#include <sstream>
#include <cstdio>
#include "comparators.h"

#include <iostream>

IX_Manager::IX_Manager(PF_Manager &pfm) : pf_man(pfm){
}

IX_Manager::~IX_Manager()
{
    
}

/*
 * Creates a new index given the filename, the index number, attribute type and length.
 */
RC IX_Manager::CreateIndex(const char *fileName, int indexNo,
                           AttrType attrType1, int attrLength1,
                           AttrType attrType2, int attrLength2,
                           AttrType attrType3, int attrLength3,
                           AttrType attrType4, int attrLength4,
                           RM_Manager &rmm)
{
    RC rc = 0;

    //check if attributes are ints or floats
    if((attrType1 == INT || attrType1 == FLOAT) && attrLength1 != 4)
        return IX_BADINDEXSPEC;
    else if ((attrType2 == INT || attrType2 == FLOAT) && attrLength2 != 4)
        return IX_BADINDEXSPEC;
    else if ((attrType3 == INT || attrType3 == FLOAT) && attrLength3 != 4)
        return IX_BADINDEXSPEC;
    else if ((attrType4 == INT || attrType4 == FLOAT) && attrLength4 != 4)
        return IX_BADINDEXSPEC;
    //return error if strings
    else if(attrType1 == STRING)
        return IX_BADINDEXSPEC;
    else if(attrType2 == STRING)
        return IX_BADINDEXSPEC;
    else if(attrType3 == STRING)
        return IX_BADINDEXSPEC;
    else if(attrType4 == STRING)
        return IX_BADINDEXSPEC;
    
    //name and create the file
    std::string file = fileName;
    file += "." + std::to_string(indexNo);
    if((rc = pf_man.CreateFile(file.c_str())))
        return rc;

    //std::cout << "PAGE MADE\n";
    //initialize the file
    PF_FileHandle fh;
    if ((rc = pf_man.OpenFile(file.c_str(), fh)))
        return rc;

    PF_PageHandle ph_header;
    PF_PageHandle ph_root;
    PageNum headerpage;
    PageNum rootpage;

    if((rc = fh.AllocatePage(ph_header)) || (rc = ph_header.GetPageNum(headerpage))
        || (rc = fh.AllocatePage(ph_root)) || (rc = ph_root.GetPageNum(rootpage)))
        return rc;
    //std::cout << "Header page = " << headerpage << "\n";
    //std::cout << "ADDING HEADER\n";
    //get header and init
    IX_IndexHeader *header;
    ph_header.GetData((char *&) header);
    header->file = file.c_str();
    header->min_entries = 2;
    header->max_entries = 5;
    header->rootPage = rootpage;
    header->attrType1 = attrType1;
    header->attrType2 = attrType2;
    header->attrType3 = attrType3;
    header->attrType4 = attrType4;
    header->attrLength1 = attrLength1;
    header->attrLength2 = attrLength2;
    header->attrLength3 = attrLength3;
    header->attrLength4 = attrLength4;
    header->totalEntries = 0;
    header->relname = fileName;
    header->rmm = &rmm;
    //std::cout << "ROOT PAGE = " << header->rootPage << "\n"; 
    //std::cout << "ADDING ROOT\n";
    IX_Node *root;
    ph_root.GetData((char *&) root);
    root->myPageNum = rootpage;
    root->isLeaf = true;
    root->isEmpty = true;
    root->isFull = false;
    root->area = 0;
    root->x1 = 0;
    root->x2 = 0;
    root->y1 = 0;
    root->y2 = 0;
    root->parent = NULL;
    RID temp;
    for(int i = 0; i < header->max_entries; i++){
        root->rids[i] = temp;
    }
    for(int i = 0; i < header->max_entries; i++){
        root->children[i] = NULL;
    }
    //std::cout << "CLOSING\n";
    if((rc = fh.MarkDirty(headerpage)) || (rc = fh.UnpinPage(headerpage)) ||
       (rc = fh.MarkDirty(rootpage)) || (rc = fh.UnpinPage(rootpage)) ||
       (rc = pf_man.CloseFile(fh)))
        return rc;
    //std::cout << "DONE\n";
    return 0;
}

/*
 * This function destroys a valid index given the file name and index number.
 */
RC IX_Manager::DestroyIndex(const char *fileName, int indexNo1, int indexNo2, int indexNo3, int indexNo4)
{
    RC rc = 0;
    if((indexNo1 - indexNo2) + (indexNo2 - indexNo4)){
        std::cout << "Attributes have different indexes. Cannot destroy\n";
        return START_SM_ERR - 1;
    }
    //std::cout << "ok\n";
    std::string file = fileName;
    file += "." + std::to_string(indexNo1);
    std::cout << "Destroying " << file << "\n";
    if((rc = pf_man.DestroyFile(file.c_str())))
        return rc;
    std::cout << "DONE\n";
    return rc;
}

/*
 * This function, given a valid fileName and index Number, opens up the
 * index associated with it, and returns it via the indexHandle variable
 */
RC IX_Manager::OpenIndex(const char *fileName, int indexNo,
                 IX_IndexHandle &indexHandle)
{
    std::cout << "OpenIndex: Start\n";
    RC rc;
    //std::cout <<"OPENING INDEX\n"; 
    std::string file = fileName;
    file += "." + std::to_string(indexNo);

    PF_FileHandle pfh;
    if ((rc = pf_man.OpenFile(file.c_str(), pfh)))
        return rc;
    std::cout << "OpenIndex: unixfd is " << pfh.unixfd << "\n";
    //std::cout << "OPENED FILE\n"; 
    //set up index handle
    PF_PageHandle ph;
    PageNum firstpage;
    char *pData;
    
    if((rc = pfh.GetFirstPage(ph)) || (ph.GetPageNum(firstpage)) || (ph.GetData(pData))){
        pfh.UnpinPage(firstpage);
        pf_man.CloseFile(pfh);
        return rc;
    }
    //std::cout << "GOT FIRST PAGE\n";

    struct IX_IndexHeader *header = (struct IX_IndexHeader *) pData;

    //std::cout << "ROOT PAGE IS = " << header->rootPage << "\n";

    if((rc = pfh.GetThisPage(header->rootPage, indexHandle.root_ph)))
        return rc;
    indexHandle.pfh = pfh;
    indexHandle.header = *header;
    indexHandle.openedIXH = true;

    //std::cout << "ROOT PAGE IS = " << header->rootPage << "\n";
    if((rc = pfh.UnpinPage(firstpage)))
        return rc;
    
    //std::cout << "OPENED INDEX\n";
    return 0;
}

/*
 * Given a valid index handle, closes the file associated with it
 */
RC IX_Manager::CloseIndex(IX_IndexHandle &indexHandle)
{
    //std::cout << "CLOSING INDEX\n";
    RC rc = 0;
    
    PF_PageHandle ph;
    char *pData;
    PageNum page;

    //std::cout << "HERERERE\n";

    PageNum root = indexHandle.header.rootPage;
    //std::cout << "OK\n";
    std::cout << "CloseIndex: Unpinning root page\n";
    
    if((rc = indexHandle.pfh.MarkDirty(root)) || (rc = indexHandle.pfh.UnpinPage(root)))
        return rc;
    std::cout << "CloseIndex: Unpinned root page\n";
    //std::cout << "DONE WITH ROOT\n";

    

    if(indexHandle.header_modified){
        if((rc = indexHandle.pfh.GetFirstPage(ph)) || ph.GetPageNum(page))
            return rc;
        if((rc = ph.GetData(pData))){
            RC rc2;
            if((rc2 = indexHandle.pfh.UnpinPage(page)))
                return rc2;
            return rc;
        }
        memcpy(pData, &indexHandle.header, sizeof(struct IX_IndexHeader));
        
        
        if((rc = indexHandle.pfh.MarkDirty(page)) || (rc = indexHandle.pfh.UnpinPage(page)))
            return rc;
    }
    //std::cout << "ALMOST DONE\n";
    if((rc = pf_man.CloseFile(indexHandle.pfh)))
        return rc;
    if(indexHandle.openedIXH == false)
        return (IX_INVALIDINDEXHANDLE);
    indexHandle.openedIXH = false;

    //std::cout << "CLOSED INDEX\n";
    return 0;
}
