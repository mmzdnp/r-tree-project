//
// File:        ix_indexhandle.cc
// Description: IX_IndexHandle handles manipulations within the index
// Author:      <Your Name Here>
//

#include <unistd.h>
#include <sys/types.h>
#include "ix.h"
#include "pf.h"
#include "comparators.h"
#include <cstdio>

#include <iostream>
#include "rm.h"

std::vector <PageNum> pagesToUnpin;

IX_IndexHandle::IX_IndexHandle()
{
  // Implement this
}

IX_IndexHandle::~IX_IndexHandle()
{
  // Implement this
}

RC IX_IndexHandle::InsertEntry(void *pData1, void *pData2, void *pData3, void *pData4, const RID &rid)
{
    std::cout << "STARTING INSERTS\n";
    RC rc;
    //get root 
    struct IX_Node *root;
    //std::cout << "GETTING ROOT\n";
    if((rc = root_ph.GetData((char *&)root)))
        return rc;
    std::cout << "InsertEntry: push_back " << header.rootPage << "\n";
    pagesToUnpin.push_back(header.rootPage);
    std::cout<< "CHOOSING LEAF\n";
    //invoke ChooseLeaf to select a leaf node in which to place object
    PageNum CL_pagenum;
    IX_Node *node = ChooseLeaf(pData1, pData2, pData3, pData4, root, CL_pagenum);
    std::cout << "CHOSE LEAF\n"; 
    //std::cout << node->isFull << "\n";
    if(CL_pagenum != 1)
    std::cout << "InsertEntry: push_back CL_pagenum: "<< CL_pagenum << "\n";
    pagesToUnpin.push_back(CL_pagenum);
    IX_Node *new_node;
    PF_PageHandle newPH; 
    PageNum new_node_page;
    bool leaf_split_occurred = false;
    if (node->isFull) {
        
        if((rc = pfh.AllocatePage(newPH) || newPH.GetPageNum(new_node_page)))
            return rc;
        //std::cout << "**********************************************new page num = " << new_node_page << "\n";
        std::cout << "InsertEntry: push back new_node_page: " << new_node_page << "\n"; 
        pagesToUnpin.push_back(new_node_page);
        newPH.GetData((char *&) new_node);
        new_node->myPageNum = new_node_page;

        std::cout <<"SPLITTING\n";
        SplitNode(node, new_node);
        leaf_split_occurred = true;
        
    }
    //add entry to empty space
    RID temp;
    int i = 0;
    for (i = 0; i < 5; i++){
        //std::cout << i << "\n";
        //PageNum p;
        //node->rids[i].GetPageNum(p);
        //SlotNum s;
        //node->rids[i].GetSlotNum(s);
        //std::cout << "PageNum = " << p << '\n';
        //std::cout << "SlotNum = " << s  << '\n';
        if (node->rids[i] == temp){ //look for empty space
            node->rids[i] = rid;
            break;
        }
    }
    std::cout << "INSERTING AT SLOT # = " << i << "\n";
    if(i == 0){ //init mbr
        node->isFull = false;
        node->isEmpty = false;
        //std::cout << *((int*)pData1) << "\n";
        //std::cout << *((int*)pData2) << "\n";
        //std::cout << *((int*)pData3) << "\n";
        //std::cout << *((int*)pData4) << "\n";
        ////node->x1 = node->max(*(int*)pData1, *(int*)pData3);
        node->y1 = node->max(*(int*)pData2, *(int*)pData4);
        node->x2 = node->min(*(int*)pData1, *(int*)pData3);
        node->y2 = node->min(*(int*)pData2, *(int*)pData4);
        node->area = (node->x1 - node->x2) * (node->y1 - node->y2);
        std::cout << "Inital area of MBR = " << node->area << " for " << CL_pagenum << "\n";
    }
    else{ //recalculate mbr
        if( i == 4){ //set node is full if new data added at 5th slot
            std::cout << "NODE IS NOW FULL\n";
            node->isFull = true;
        }
        else
            node->isFull = false;
        node->isEmpty = false;
        node->x1 = node->max(*(int*)pData1, *(int*)pData3, node->x1);
        node->y1 = node->max(*(int*)pData2, *(int*)pData4, node->y1);
        node->x2 = node->min(*(int*)pData1, *(int*)pData3, node->x2);
        node->y2 = node->min(*(int*)pData2, *(int*)pData4, node->y2);
        node->area = (node->x1 - node->x2) * (node->y1 - node->y2);
        
        std::cout <<"MBR is : " <<node->x1 << " " <<node->y1 << " " << node->x2 << " " << node->y2 << "\n";
        std::cout << "MBR area = " << node->area << " for "<< CL_pagenum <<"\n";
    }

    //adjust tree
    if((rc = AdjustTree(node, new_node, newPH, leaf_split_occurred)))
        return rc;

    std::cout << "Pagenum = " << CL_pagenum << "\n";
    /*if((rc = pfh.MarkDirty(CL_pagenum)) || (rc = pfh.UnpinPage((CL_pagenum)))) 
        return rc;*/

    //if((CL_pagenum != header.rootPage) && (rc = pfh.UnpinPage(CL_pagenum))){
        //return rc;
    //}

    /*if(leaf_split_occurred){
        std::cout << "UNPINNING PAGE = " << new_node_page << "\n";
        if((rc = pfh.MarkDirty(new_node_page)) || (rc = pfh.UnpinPage(new_node_page)))
                return rc;
    }*/

    sort(pagesToUnpin.begin(), pagesToUnpin.end());
    pagesToUnpin.erase( unique(pagesToUnpin.begin(), pagesToUnpin.end()), pagesToUnpin.end());
    for(int i = 0; i < pagesToUnpin.size(); i++){
        std::cout << "InsertEntry: Unpinning page = " << pagesToUnpin.at(i) << "\n";
        if((rc = pfh.MarkDirty(pagesToUnpin.at(i))) || (rc = pfh.UnpinPage(pagesToUnpin.at(i))))
                return rc;
        std::cout << "InsertEntry: Unpinned page = " << pagesToUnpin.at(i) << "\n";
    }
    pagesToUnpin.clear();
    //std::cout << "Test\n";
    //if(leaf_split_occurred){
       // std::cout << "Save\n";
        //if((rc = pfh.MarkDirty(new_node_page)))// || (rc = pfh.UnpinPage(new_node_page)))
          //  return rc;
   // }

  /////DO NOT REMOVE/////
  /*std::cout << "Testing get rec\n";
  
  RM_FileHandle rfh;
  header.rmm->OpenFile(header.relname, rfh);
  RM_Record rec;
  char *pData;
  
  if((rc = rfh.GetRec(rid, rec)))
    return rc;
  
  std::cout << "rec size = " << rec.size << "\n";
  int d[4];
  for(int a = 0; a < 4; a++){
    d[a] = *(rec.data + (4 * a));
    std::cout << "rec data = " << d[a] << " ";
  }
  std::cout << "\n";
}*/
    return 0;
}

RC IX_IndexHandle::DeleteEntry(void *pData1, void *pData2, void *pData3, void *pData4, const RID &rid)
{
    std::cout << "DeleteEntry: Before getting root \n"; 
    RC rc;
    //get root 
    struct IX_Node *root;
    if((rc = root_ph.GetData((char *&)root)))
        return rc;
    std::cout << "DeleteEntry: Pushing back page : " << header.rootPage << "\n";
    pagesToUnpin.push_back(header.rootPage);


    IX_Node *check;
    check = FindLeaf(root, pData1, pData2, pData3, pData4);
    if(check == NULL)
    {
        std::cerr <<"Delete: Record not found.\n";
        return -1;
    }
    else
    {   
        RM_FileHandle rfh;
        header.rmm->OpenFile(header.relname, rfh);
        RM_Record rec;
        for(int i = 0; i < 5; i++)
        {
            int data[4];
            if(rc =(rfh.GetRec(check->rids[i], rec)))
                return rc;
            std:: cout << "DeleteEntry: check leaf data ";
            for(int a = 0; a < 4; a++)
            {
                data[a] = *(rec.data + (4 * a));
                std::cout << data[a] << " ";
            }
            std::cout << "\n";

            int Emin_x = check->min(*(int*)pData1, *(int*)pData3);
            int Emin_y = check->min(*(int*)pData2, *(int*)pData4);
            int Emax_x = check->max(*(int*)pData1, *(int*)pData3);
            int Emax_y = check->max(*(int*)pData2, *(int*)pData4);

            int min_x = check->min(data[0],data[2]);
            int min_y = check->min(data[1],data[3]);
            int max_x = check->max(data[0],data[2]);
            int max_y = check->max(data[1],data[3]);
    
            if(min_x == Emin_x && min_y == Emin_y && max_x == Emax_x && max_y == Emax_y)
            {
                std::cout << "DeleteEntry: Found match\n";
                //removing entry from node and restructure rids array
                RID temp;
                for(int j = 4; j >= 0; j--)
                {
                    if(!(check->rids[j] == temp))
                    {
                        check->rids[i] = check->rids[j];
                        
                        std::cout << "DeleteEntry: After swap check->rids[" << i << "] has page " << check->rids[i].page << "\n";
                        
                        check->rids[j] = temp;
                        
                        if((rc = pfh.MarkDirty(1)) || (rc = pfh.UnpinPage(1)))
                           return rc;                                                
                        pfh.FlushPages();

                        if(rc = pfh.GetThisPage(1, root_ph))
                            return rc;
 
                        struct IX_Node *r;
                        if((rc = root_ph.GetData((char *&)r)))
                            return rc;
 
                        
                        for(int k = 0; k < 5; k++)
                        {
                            std::cout << "\nDeleteEntry: Checking root rids: r->rids[" << k <<"] page: " << r->rids[k].page<<"\n";
                        }
                        break;
                    }
                }
                break; 
            }
        }
    }
    CondenseTree(check);
    int countChildren =0;
    int lastChildIndex =0;
    for(int i = 0; i< 5; i++)
    {
        if(root->children[i] == NULL)
        {
            lastChildIndex = i;
            break;
        }
        countChildren++;
    }
    //if the root node has only one child after the tree has been adjusted, make the child
    //the new root
    if(countChildren == 1)
    {
        IX_Node *tmp = root->children[lastChildIndex];
        root->isLeaf = tmp->isLeaf;
        root->isEmpty = tmp->isEmpty;
        root->isFull = tmp->isFull;
        root->area = tmp->area;
        root->x1 = tmp->x1;
        root->x2 = tmp->x2;
        root->y1 = tmp->y1;
        root->y2 = tmp->y2;
        root->parent = NULL;
        for(int i = 0; i < 5; i++){
            root->children[i] = tmp->children[i];
            root->rids[i] = tmp->rids[i];
        }
        PageNum pageN;
        if(rc = (root->children_ph[lastChildIndex].GetPageNum(pageN)))
            return rc;
        std::cout << "Delete: Disposing page " << pageN << "\n";
        if(rc = pfh.DisposePage(pageN))
            return rc;
    }
  
                        struct IX_Node *r;
                        if((rc = root_ph.GetData((char *&)r)))
                            return rc;

        for(int k = 0; k < 5; k++)
                        {
                            std::cout << "\nDeleteEntry: Checking root rids: r->rids[" << k <<"] page: " << r->rids[k].page<<"\n";
                        }


    sort(pagesToUnpin.begin(), pagesToUnpin.end());
    pagesToUnpin.erase( unique(pagesToUnpin.begin(), pagesToUnpin.end()), pagesToUnpin.end());
    for(int i = 0; i < pagesToUnpin.size(); i++){
        std::cout << "DeleteEntry: Unpinning page = " << pagesToUnpin.at(i) << "\n";
        if((rc = pfh.MarkDirty(pagesToUnpin.at(i))) || (rc = pfh.UnpinPage(pagesToUnpin.at(i))))
                return rc;
    }

    pfh.FlushPages();
    pagesToUnpin.clear();
    return 0; 
}


RC IX_IndexHandle::ForcePages()
{
    RC rc = 0;
    if((rc = pfh.ForcePages()))
        return rc;
    return rc;
}


//private functions
IX_Node * IX_IndexHandle::ChooseLeaf(void *pData1, void *pData2, void *pData3, void *pData4, IX_Node *root, PageNum &CL_pagenum)
{
    RC rc = 0;
   if(root->isLeaf){
       if(root->parent == NULL)
           CL_pagenum = header.rootPage;
       std::cout << "FOUND LEAF page #= " << CL_pagenum << "\n"; 
       
       PF_PageHandle p;
       if((rc = pfh.GetThisPage(CL_pagenum, p)))
           std::cout << "Could not get page # " << CL_pagenum << "\n";
        
       std::cout << "ChooseLeaf: push back CL_pagenum: "<<CL_pagenum<<"\n";
       pagesToUnpin.push_back(CL_pagenum);
       return root;
   }
   else{
        double arr[5];
        int valid = 0;
        IX_Node *children[5];
        PageNum pages[5];
        struct IX_Node *child;
        for(int i = 0; i < 5; i++){
            //std::cout << i << "\n";
            if(root->children[i] != NULL){
                PageNum new_node_page;
                PF_PageHandle newPH;
                //std::cout << "new page num = " << new_node_page << "\n";
                
                //calculate the enlargement area for adding entry to child node
                
                if((rc = root->children_ph[i].GetPageNum(new_node_page)) || (rc = pfh.GetThisPage(new_node_page, root->children_ph[i])) || (rc = root->children_ph[i].GetData((char *&) child)))
                    std::cout << "DOESNT WORK\n";
                //if((rc = pfh.GetThisPage(2, newPH)) || (rc = newPH.GetData((char *&) child)))
                    //std::cout << "DOESNT WORK\n";
                //std::cout << "Got data\n";
                //std::cout << root->children[i]->isLeaf << "\n";
                //std::cout << "AREA = " << child->area << "\n";
                //std::cout << "Calc Area\n";
                arr[i] = CalcArea(pData1, pData2, pData3, pData4, child) - child->area;//root->children[i]->area;
                //std::cout << "Got area\n";
                valid++;
                children[i] = child;
                pages[i] = new_node_page;
                //if(new_node_page != 3){
                std::cout << "ChooseLeaf: Unpinning page: " << new_node_page << "\n";
                    if((rc = pfh.UnpinPage(new_node_page)))
                        std::cout << "FAILED TO UNPIN PAGE # " << new_node_page << "\n";
                //}
                std::cout << "ChooseLeaf: Unpinned page " << new_node_page << "\n";
            }
        }
        int min = 0;
        //get node with the minimum enlargement
        for(int i = 0; i < valid; i++){
            if(arr[i] < arr[min])
                min = i;
        }
        //recurse on the best child node
        CL_pagenum = pages[min];
        ChooseLeaf(pData1, pData2, pData3, pData4, children[min], CL_pagenum);//root->children[min]);
   }
}

double IX_IndexHandle::CalcArea(void *pData1, void *pData2, void *pData3, void *pData4, IX_Node *node){
    int pd1 = *(int*) pData1;
    int pd2 = *(int*) pData2;
    int pd3 = *(int*) pData3;
    int pd4 = *(int*) pData4;
   
    //std::cout << "Getting node max and mins\n";
    //std::cout << node->isLeaf << "\n";
    double x1 = node->max(node->x1,node->x2);
    double x2 = node->min(node->x1,node->x2);
    double y1 = node->max(node->y1,node->y2);
    double y2 = node->min(node->y1,node->y2);
    //std::cout << "Got max and mins\n";
    x1 = node->max(pd1, pd3, x1);
    y1 = node->max(pd2, pd4, y1);
    
    x2 = node->min(pd1, pd3, x2);
    y2 = node->min(pd2, pd4, y2);

    return ((x1 - x2) * (y1 - y2));
}

//Quadratic Split Algorithm
RC IX_IndexHandle::SplitNode(IX_Node *node, IX_Node *new_node)
{
    RC rc = 0;
    int entry1;
    int entry2;
    std::cout << "Picking Seeds\n";
    if((rc = PickSeeds(node, entry1, entry2)))
        return rc;
    //if(entry1 == entry2)
        //std::cout << "ENTRIES EQUAL\n";
    RID t;
    RID group1[3];
    group1[0] = (node->rids[entry1]);
    group1[1] = t;
    group1[2] = t;
    RID group2[3];
    group2[0] = (node->rids[entry2]);
    group2[1] = t;
    group2[2] = t;
    bool used[5];
    for(int i = 0; i < 5; i++)
        used[i] = false;
    //std::cout << "entry1 = " << entry1 << " entry2 = " << entry2 << "\n";
    used[entry1] = true;
    used[entry2] = true;
    int size_g1 = 1;
    int size_g2 = 1;
    for(int i = 0; i < 3; i++){ //find where to place the next two entries   
        int next = 0;
        //std::cout << i << " " << size_g1 << " " << size_g2 << "\n";
        
        if(size_g2 == 1 && size_g1 == 3){ //add last entry to group2 to fulfill minimum requirement
            std::cout << "min requirement g2\n";
            for(int j = 0; j < 5; j++){
                if(!used[j]){
                    group2[1] = node->rids[j];
                    size_g2++;
                    break;
                }
            }
        }
        else if(size_g1 == 1 && size_g2 == 3){ //add last entry to group 1 to fulfill minimum requirement
            std::cout << "min requirement g1\n";
            for(int j = 0; j < 5; j++){
                if(!used[j]){
                    group1[1] = node->rids[j];
                    size_g1++;
                    break;
                }
            }
        }
        if((rc = PickNext(*node, group1, group2, used, next, size_g1, size_g2)))
            return rc;
        used[next] = true;
    }

    //assign groups to the nodes
    node->isFull = false;
    *new_node = *node;
    RID temp;
    RM_FileHandle rfh;
    header.rmm->OpenFile(header.relname, rfh);
    RM_Record rec;
    for(int i = 0; i < 5; i++)
        node->rids[i] = temp;
    std::cout << "Num of entries on g2 = " << size_g2 << "\n";
    for(int i = 0; i < size_g2; i++){ //assign group1 to original node
        node->rids[i] = group2[i];
        //determine new MBR coordinates
        int data[4];
        if((rc = rfh.GetRec(node->rids[i], rec)))
            return rc;
        for(int j = 0; j < 4; j++)
            data[j] = *(rec.data + (4 * j));
       
        if(i == 0){
            node->x1 = node->max(data[0], data[2]); 
            node->y1 = node->max(data[1], data[3]);
            node->x2 = node->min(data[0], data[2]);
            node->y2 = node->min(data[1], data[3]);
            node->area = (node->x1 - node->x2) * (node->y1 - node->y2);
        }
        else{
            node->x1 = node->max(node->x1, data[0], data[2]); 
            node->y1 = node->max(node->y1, data[1], data[3]);
            node->x2 = node->min(node->x2, data[0], data[2]);
            node->y2 = node->min(node->y2, data[1], data[3]);
            node->area = (node->x1 - node->x2) * (node->y1 - node->y2);
        }
    }

    std::cout << "G2 MBR IS: " << node->x1 << " " <<node->y1 << " " << node->x2 << " " << node->y2 << "\n";
    std::cout << "node area = " << node->area << "\n";
    for(int i = size_g2; i < 5; i++) 
        node->rids[i] = temp;
    std::cout << "Num of entries in g1 = " << size_g1 << "\n";
    for(int i = 0; i < size_g1; i++){ //assign group2 to new node
        new_node->rids[i] = group1[i];
        //determine new MBR coordinates
        int data[4];
        if((rc = rfh.GetRec(new_node->rids[i], rec))){
            std::cout << "error\n";
            return rc;
        }
        for(int j = 0; j < 4; j++)
            data[j] = *(rec.data + (4 * j));
        if(i == 0){
            new_node->x1 = new_node->max(data[0], data[2]); 
            new_node->y1 = new_node->max(data[1], data[3]);
            new_node->x2 = new_node->min(data[0], data[2]);
            new_node->y2 = new_node->min(data[1], data[3]);
            new_node->area = (new_node->x1 - new_node->x2) * (new_node->y1 - new_node->y2);
        }
        else{
            new_node->x1 = new_node->max(new_node->x1, data[0], data[2]); 
            new_node->y1 = new_node->max(new_node->y1, data[1], data[3]);
            new_node->x2 = new_node->min(new_node->x2, data[0], data[2]);
            new_node->y2 = new_node->min(new_node->y2, data[1], data[3]);
            new_node->area = (new_node->x1 - new_node->x2) * (new_node->y1 - new_node->y2);
        } 
    }

    std::cout << "G1 MBR IS: " <<new_node->x1 << " " <<new_node->y1 << " " << new_node->x2 << " " << new_node->y2 << "\n";
    std::cout << "new node area = " << new_node->area << "\n";
    for(int i = size_g1; i < 5; i++) 
        new_node->rids[i] = temp;

    std::cout << "Node area = " << node->area << "\n";
    std::cout << "New Node area = " << new_node->area << "\n";
    if(node->parent == NULL)
        std::cout << "TRUE!\n";
    return rc;
}

RC IX_IndexHandle::PickSeeds(IX_Node *node, int &e1, int &e2)
{
    RC rc = 0;
    double largest_so_far = -1;
    int index1 = 0;
    int index2 = 0;
    RM_FileHandle rfh;
    header.rmm->OpenFile(header.relname, rfh);
    RM_Record rec;
    for(int i = 0; i < 5; i++){
        int data[4];
        RM_FileHandle rfh;
        header.rmm->OpenFile(header.relname, rfh);
        RM_Record rec;
        double maxX, minX, maxY, minY;   
        
        if(node->isLeaf && node->children[0] == NULL){ //leaf
            if((rc = rfh.GetRec(node->rids[i], rec)))
              return rc;
              
            for(int a = 0; a < 4; a++){
              data[a] = *(rec.data + (4 * a));
            }

            maxX = node->max(data[0], data[2]);
            minX = node->min(data[0], data[2]);
            maxY = node->max(data[1], data[3]);
            minY = node->min(data[1], data[3]);
        }
        
        else{ //nonleaf
            std::cout << "non leaf split in pick seeds\n";
            //get child mbr
            IX_Node *child;
            PageNum child_page;
            PF_PageHandle newPH;
            
            //get child's data
            std::cout << "child page = " << node->children_ph[i].pageNum << "\n";
            if((rc = node->children_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, node->children_ph[i])) || (rc = node->children_ph[i].GetData((char *&) child)))
                std::cout << "DOESNT WORK\n";
            std::cout << "CAN ACCESS CHILDREN_PH\n"; 
            //set max and min x y
            //pagesToUnpin.push_back(child_page);
            maxX = child->x1;
            minX = child->x2;
            maxY = child->y1;
            minY = child->y2;
            
            //unpin
            std::cout << "Pickseeds nonleaf: Unpining page: " << child_page << "\n";
            if((rc = pfh.UnpinPage(child_page)))
                std::cout << "FAILED TO UNPIN PAGE # " << child_page << "\n";
            std::cout << "Pickseeds nonleaf: Unpinned page: " << child_page << "\n";
        }
        double area1 = (maxX - minX) * (maxY - minY); 
        
        for(int j = 0; j < 5; j++){
            if(j <= i)
                continue;
            //std::cout << i << ", " << j << "\n";
            int data2[4];
            double maxX2, minX2, maxY2, minY2;
            
            if(node->isLeaf){ 
                if((rc = rfh.GetRec(node->rids[j], rec)))
                    return rc;
              
                for(int a = 0; a < 4; a++){
                    data2[a] = *(rec.data + (4 * a));
                }
                maxX2 = node->max(data2[0], data2[2]);
                minX2 = node->min(data2[0], data2[2]);
                maxY2 = node->max(data2[1], data2[3]);
                minY2 = node->min(data2[1], data2[3]);
            }

            else{
                //get child mbr
                IX_Node *child;
                PageNum child_page;
                PF_PageHandle newPH;
                
                //get child's data
                if((rc = node->children_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, child->children_ph[i])) || (rc = node->children_ph[i].GetData((char *&) child)))
                    std::cout << "DOESNT WORK\n";
                
                //set max and min x y
                maxX2 = child->x1;
                minX2 = child->x2;
                maxY2 = child->y1;
                minY2 = child->y2;
                
                //unpin
                std::cout << "Pickseeds leaf: Unpinning page : " << child_page << "\n";
                if((rc = pfh.UnpinPage(child_page)))
                    std::cout << "FAILED TO UNPIN PAGE # " << child_page << "\n";
                std::cout << "Pickseeds leaf: Unpinning page : " << child_page << "\n";
            }
            
            double area2 = (maxX2 - minX2) * (maxY2 - minY2);
            //std::cout << data2[0] << " " << data2[1] << " " << data2[2] << " " << data2[3] << "\n";
            //std::cout << "area2 = " << area2 << "\n";   
            double maxXJ, minXJ, maxYJ, minYJ;
            maxXJ = node->max(maxX, maxX2);
            minXJ = node->min(minX, minX2);
            maxYJ = node->max(maxY, maxY2);
            minYJ = node->min(minY, minY2); 
            
            double areaJ = (maxXJ - minXJ) * (maxYJ - minYJ);
            //std::cout << "areaJ = " << areaJ << "\n";
            double d = areaJ - area1 - area2;
            //std::cout << "d = " << d << "\n";
            if(d > largest_so_far){
                largest_so_far = d;
                index1 = i;
                index2 = j;
            }
        }
    }
    e1 = index1;
    e2 = index2;
    return rc;
}

RC IX_IndexHandle::PickNext(IX_Node node, RID group1[], RID group2[],bool used[], int &next, int &size_g1, int &size_g2){
    RC rc;

    RM_FileHandle rfh;
    header.rmm->OpenFile(header.relname, rfh);
    RM_Record rec; 

    //get area of group1
    double area_g1;
    int data_g1[4];
    double maxX1, minX1, maxY1, minY1;
    RID t;
    for(int i = 0; i < 3; i++){
        if(group1[i] == t)
            break;
        else{
            if((rc = rfh.GetRec(group1[i], rec)))
                return rc;
            for(int j = 0; j < 4; j++)
                data_g1[j] = *(rec.data + (4 * j));
            if(i == 0){
                maxX1 = node.max(data_g1[0], data_g1[2]);
                minX1 = node.min(data_g1[0], data_g1[2]);
                maxY1 = node.max(data_g1[1], data_g1[3]);
                minY1 = node.min(data_g1[1], data_g1[3]);
                
            }
            else{
                maxX1 = node.max(maxX1, data_g1[0], data_g1[2]);
                minX1 = node.min(minX1, data_g1[0], data_g1[2]);
                maxY1 = node.max(maxY1, data_g1[1], data_g1[3]);
                minY1 = node.min(minY1, data_g1[1], data_g1[3]);
            }
            area_g1 = (maxX1 - minX1) * (maxY1 - minY1);
        }
    }

    //get area of group2 
    double area_g2;
    int data_g2[4];
    double maxX2, minX2, maxY2, minY2;
    for(int i = 0; i < 3; i++){
        if(group2[i] == t)
            break;
        else{
            if((rc = rfh.GetRec(group2[i], rec)))
                return rc;
            for(int j = 0; j < 4; j++)
                data_g1[j] = *(rec.data + (4 * j));
            if(i == 0){
                maxX2 = node.max(data_g2[0], data_g2[2]);
                minX2 = node.min(data_g2[0], data_g2[2]);
                maxY2 = node.max(data_g2[1], data_g2[3]);
                minY2 = node.min(data_g2[1], data_g2[3]);
                
            }
            else{
                maxX2 = node.max(maxX2, data_g2[0], data_g2[2]);
                minX2 = node.min(minX2, data_g2[0], data_g2[2]);
                maxY2 = node.max(maxY2, data_g2[1], data_g2[3]);
                minY2 = node.min(minY2, data_g2[1], data_g2[3]);
            }
            area_g2 = (maxX2 - minX2) * (maxY2 - minY2);
        }
    }

    double d1_best_so_far = -1;
    double d2_best_so_far = -1;
    int index1 = -1;
    int index2 = -1;
    //loop through the data records
    for(int i = 0; i < 5; i++){
        if(used[i]) //skip data records already in a group
            continue;
        else{
            //get the node data records
            int data[4];
            if((rc = rfh.GetRec(node.rids[i], rec)))
                return rc;
            for(int j = 0; j < 4; j++)
                data[j] = *(rec.data + (4 * j));


            double d1 = 0;
            double d2 = 0;
            double maxX, minX, maxY, minY;
            maxX = node.max(data[0], data[2]);
            minX = node.min(data[0], data[2]);
            maxY = node.max(data[1], data[3]);
            minY = node.min(data[1], data[3]);

            maxX = node.max(maxX, maxX1);
            minX = node.min(minX, minX1);
            maxY = node.max(maxY, maxY1);
            minY = node.min(minY, minY1);

            d1 = ((maxX - minX) * (maxY - minY)) - area_g1; 
            if(d1_best_so_far == -1){
                d1_best_so_far = d1;
                index1 = i;
            }
            else if(d1 < d1_best_so_far){
                d1_best_so_far = d1;
                index1 = i;
            }
            
            maxX = node.max(data[0], data[2]);
            minX = node.min(data[0], data[2]);
            maxY = node.max(data[1], data[3]);
            minY = node.min(data[1], data[3]);

            maxX = node.max(maxX, maxX2);
            minX = node.min(minX, minX2);
            maxY = node.max(maxY, maxY2);
            minY = node.min(minY, minY2);

            d2 = ((maxX - minX) * (maxY - minY)) - area_g2;
            if(d2_best_so_far == -1){
                d2_best_so_far = d2;
                index2 = i;
            }
            else if(d2 < d2_best_so_far){
                d2_best_so_far = d2;
                index2 = i;
            }
        }
    }

    //Add next entry to group
    if(d1_best_so_far < d2_best_so_far){
        //insert at empty slot
        for(int i = 0; i < 3; i++){
            if(group1[i] == t){
                group1[i] = node.rids[index1];
                next = index1;
                size_g1++;
                break;
            }
        }
    }
    else{
        //insert at empty slot
        for(int i = 0; i < 3; i++){
            if(group2[i] == t){
                group2[i] = node.rids[index2];
                next = index2;
                size_g2++;
                break;
            }
        }
    }

    return rc;
}

RC IX_IndexHandle::AdjustTree(IX_Node *node, IX_Node *new_node, PF_PageHandle newPH, bool leaf_split_occurred){
    std::cout << "Adjust tree\n";
    RC rc = 0;

    if(!node->isLeaf){
        std::cout << "Something is wrong\n";
    }
    

    if(node->isLeaf && node->parent == NULL && !leaf_split_occurred){ //if node is a leaf node and is the root
        std::cout << "Before first split\n";
        return 0;
    }

    //first split where root is still a leaf
    //make node a non-leaf root node
    //create a new leaf node that holds the data that node used to
    //point this new node and the passed in new node to the root
    std::cout << "node's parent pagenum = " << node->parent_pagenum << "\n";
    if(node->parent == NULL && leaf_split_occurred){ 
        std::cout << "first split\n";
        IX_Node *atnode;
        PageNum atnode_page;
        PF_PageHandle atPH;
        if((rc = pfh.AllocatePage(atPH) || atPH.GetPageNum(atnode_page)))
            return rc;
        //std::cout << "**********************************************atnode_page = " << atnode_page << "\n";
        atPH.GetData((char *&) atnode);
        
        std::cout << "AdjustTree: push back atnode_page: "<<atnode_page<<"\n";
        pagesToUnpin.push_back(atnode_page);
        //atnode = node;
        atnode->myPageNum = atnode_page;
        atnode->isLeaf = true;
        atnode->isEmpty = false;
        atnode->isFull = false;
        atnode->area = node->area;
        atnode->x1 = node->x1;
        atnode->x2 = node->x2;
        atnode->y1 = node->y1;
        atnode->y2 = node->y2;
        for(int i = 0; i < 5; i++){
            atnode->children[i] = NULL;
            atnode->rids[i] = node->rids[i];
        }

        node->isLeaf = false;
        node->children[0] = atnode;
        node->children_ph[0] = atPH;
        std::cout << "atPH pagenum = " << atPH.pageNum << "\n";
        std::cout << "atnode mypagenum = " << atnode->myPageNum << "\n";
        node->children[1] = new_node;
        node->children_ph[1] = newPH;
        std::cout << "newPH pagenum = " << newPH.pageNum << "\n";
        std::cout << "newnode mypagenum = " << new_node->myPageNum << "\n";
        RID temp;
        for(int i = 0; i < 5; i++){
            node->rids[i] = temp;
        }
        atnode->parent = node;
        atnode->parent_pagenum = node->myPageNum;
        new_node->parent = node;
        new_node->parent_pagenum = atnode->myPageNum;
        CalcMBR(node, atnode, new_node);
        //std::cout << "save in split\n";
        //std::cout << "Adjust Tree: Unpinning page: " << atnode_page << "\n";
        if((rc = pfh.MarkDirty(atnode_page)))// || (rc = pfh.UnpinPage(atnode_page)))
            return rc;
        //std::cout << "Adjust Tree: Unpinned page: " << atnode_page << "\n";
        return 0;
    }

    

    //adjust tree for non leaf nodes after initial split
    bool propogate = leaf_split_occurred;

    PF_PageHandle ph;

    IX_Node *p = node->parent;
    std::cout << "child node's stored parent pagenum = " << node->parent_pagenum << "\n";
    if((rc = pfh.GetThisPage(node->parent_pagenum, ph)) || (rc = ph.GetData((char *&) p)))
        return rc;
    std::cout << "parent ph pageNum = " << ph.pageNum << "\n";

    while(node->parent != NULL && propogate){ //while current node examined is not the root and there is a need to propogate up
        SplitNode_NonLeaf(node, new_node, newPH, propogate);
    }
    if(propogate && node->parent == NULL){ //split propogated to root, make new root
        IX_Node *atnode;
        PageNum atnode_page;
        PF_PageHandle atPH;
        if((rc = pfh.AllocatePage(atPH) || atPH.GetPageNum(atnode_page)))
            return rc;
        atPH.GetData((char *&) atnode);
        
        
        std::cout <<"AdjustTree propagate: push back atnode_page: " << atnode_page << "\n";

        pagesToUnpin.push_back(atnode_page);
        *atnode = *node;
        atnode->myPageNum = atnode_page;
        //make node new root
        node->isLeaf = false;
        node->children[0] = atnode;
        node->children_ph[0] = atPH;
        node->children[1] = new_node;
        node->children_ph[1] = newPH;
        for(int i = 2; i < 5; i++){
            node->children[i] = NULL;
        }
        RID temp;
        for(int i = 0; i < 5; i++)
            node->rids[i] = temp;
        atnode->parent = node;
        atnode->parent_pagenum = node->parent_pagenum;
        new_node->parent = node;
        new_node->parent_pagenum = atnode->parent_pagenum;
        CalcMBR(node, atnode, new_node);
        //std::cout << "save in split\n";
        std::cout << "Adjust Tree propagate: Unpinning page: " << atnode_page << "\n";
        if((rc = pfh.MarkDirty(atnode_page))|| (rc = pfh.UnpinPage(atnode_page)))
            return rc;
        return 0;
    }

    return rc;
}

void IX_IndexHandle::CalcMBR(IX_Node *dest, IX_Node *src1, IX_Node *src2){
    //std::cout << src1->x1 << " " << src2->x1 << "\n";
    //std::cout << src1->y1 << " " << src2->y1 << "\n";
    //std::cout << src1->x2 << " " << src2->x2 << "\n";
    //std::cout << src1->y2 << " " << src2->y2 << "\n";
    dest->x1 = dest->max(src1->x1, src2->x1);
    dest->y1 = dest->max(src1->y1, src2->y1);
    dest->x2 = dest->min(src1->x2, src2->x2);
    dest->y2 = dest->min(src1->y2, src2->y2);
    dest->area = (dest->x1 - dest->x2) * (dest->y1 - dest->y2);
    std::cout << "Area of MBR= " << dest->area << "\n";
}

RC IX_IndexHandle::SplitNode_NonLeaf(IX_Node *n, IX_Node *nn, PF_PageHandle &nnPH, bool &propogate){
    RC rc = 0;
    PF_PageHandle ph;
    std::cout << "IN SPLITNODE_NONLEAF\n";
    IX_Node *p = n->parent;
    if((rc = pfh.GetThisPage(n->parent_pagenum, ph)) || (rc = ph.GetData((char *&) p)))
        return rc;
    std::cout << "parent ph pageNum = " << ph.pageNum << "\n"; 
    if(propogate){ //a previous split
        if(p->isFull){ //split the parent
            int entry1;
            int entry2;
            std::cout << "non leaf split pick seeds\n";
            PickSeeds(p, entry1, entry2);

            IX_Node *group1[3];
            PF_PageHandle group1_ph[3];
            group1[0] = (p->children[entry1]);
            group1[1] = NULL;
            group1[2] = NULL;
            group1_ph[0] = p->children_ph[entry1];
            IX_Node *group2[3];
            PF_PageHandle group2_ph[3];
            group2[0] = (p->children[entry2]);
            group2[1] = NULL; 
            group2[2] = NULL;
            group2_ph[0] = p->children_ph[entry2];
            bool used[5];
            for(int i = 0; i < 5; i++)
                used[i] = false;
            
            used[entry1] = true;
            used[entry2] = true;
            int size_g1 = 1;
            int size_g2 = 1;

            for(int i = 0; i < 3; i++){ //find where to place the next two entries   
                int next = 0;
            
                if(size_g2 == 1 && size_g1 == 3){ //add last entry to group2 to fulfill minimum requirement
                    std::cout << "min requirement g2\n";
                    for(int j = 0; j < 5; j++){
                        if(!used[j]){
                            group2[1] = p->children[j];
                            group2_ph[1] = p->children_ph[j];
                            size_g2++;
                            break;
                        }
                    }
                }
                else if(size_g1 == 1 && size_g2 == 3){ //add last entry to group 1 to fulfill minimum requirement
                    std::cout << "min requirement g1\n";
                    for(int j = 0; j < 5; j++){
                        if(!used[j]){
                            group1[1] = p->children[j];
                            group1_ph[1] = p->children_ph[j];
                            size_g1++;
                            break;
                        }
                    }
                }
                if((rc = PickNext_NonLeaf(*p, group1, group2, group1_ph, group2_ph, used, next, size_g1, size_g2)))
                    return rc;
                used[next] = true;
            }
                

            //add the previously split node to the group with less entries
            if(size_g1 < size_g1){
                size_g1++;
                group1[2] = nn;
                group1_ph[2] = nnPH;
            }
            else{
                size_g2++;
                group2[2] = nn;
                group2_ph[2] = nnPH;
            }


            //assign groups to the nodes
            p->isFull = false;
            IX_Node *new_p = p;
            PageNum new_p_page;
            PF_PageHandle new_p_ph;
            if((rc = pfh.AllocatePage(new_p_ph) || new_p_ph.GetPageNum(new_p_page)))
                return rc;
            new_p->myPageNum = new_p_page;

            std::cout << "Num of entries on g2 = " << size_g2 << "\n";
            for(int i = 0; i < size_g2; i++){ //assign group1 to original node
                p->children[i] = group2[i];
                p->children_ph[i] = group2_ph[i];
                //determine new MBR coordinates
                IX_Node *child;
                PageNum child_page;
                if((rc = group2_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, group2_ph[i])) || (rc = group2_ph[i].GetData((char *&) child)))
                    std::cout << "DOESNT WORK\n";

                if(i == 0){
                    p->x1 = child->x1; 
                    p->y1 = child->y1;
                    p->x2 = child->x2;
                    p->y2 = child->y2;
                    p->area = child->area;
                }
                else{
                    p->x1 = p->max(p->x1, child->x1); 
                    p->y1 = p->max(p->y1, child->y1);
                    p->x2 = p->min(p->x2, child->x2);
                    p->y2 = p->min(p->y2, child->y2);
                    p->area = (p->x1 - p->x2) * (p->y1 - p->y2);
                }
                std::cout << "Split Nonleaf: Unpinning page: " << child_page <<"\n";
                if((rc = pfh.UnpinPage(child_page)))
                    return rc;
            }
            for(int i = size_g2; i < 5; i++) 
                p->children[i] = NULL;

            std::cout << "Num of entries in g1 = " << size_g1 << "\n";
            for(int i = 0; i < size_g1; i++){ //assign group2 to new node
                new_p->children[i] = group1[i];
                new_p->children_ph[i] = group1_ph[i];
                //determine new MBR coordinates
                IX_Node *child;
                PageNum child_page;
                if((rc = group1_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, group1_ph[i])) || (rc = group1_ph[i].GetData((char *&) child)))
                    std::cout << "DOESNT WORK\n";

                if(i == 0){
                    new_p->x1 = child->x1; 
                    new_p->y1 = child->y1;
                    new_p->x2 = child->x2;
                    new_p->y2 = child->y2;
                    new_p->area = child->area;
                }
                else{
                    new_p->x1 = new_p->max(new_p->x1, child->x1); 
                    new_p->y1 = new_p->max(new_p->y1, child->y1);
                    new_p->x2 = new_p->min(new_p->x2, child->x2);
                    new_p->y2 = new_p->min(new_p->y2, child->y2);
                    new_p->area = (new_p->x1 - new_p->x2) * (new_p->y1 - new_p->y2);
                } 
                std::cout << "Split Nonleaf after new MBR coor: Unpinning child page: " << child_page << "\n";
                if((rc = pfh.UnpinPage(child_page)))
                    return rc;
            }
            for(int i = size_g1; i < 5; i++) 
                new_p->children[i] = NULL;
            std::cout << "Split Nonleaf: Unpinning new_p_page: " << new_p_page << "\n";
            if((rc = pfh.MarkDirty(new_p_page)) || (rc = pfh.UnpinPage(new_p_page)))
                return rc;

            n = p;
            nn = new_p;
            nnPH = new_p_ph;
            propogate = true;
        }


        else{
            propogate = false;
            for(int i = 0; i < 5; i++){ //find empty child slot
                if(p->children[i] == NULL){
                    p->children[i] = nn;
                    p->children_ph[i] = nnPH;
                    if(i == 4) //parent is full of children
                        p->isFull = true;
                    break;
                }
            }
        }
    }
    else
        propogate = false;
    std::cout << "Splot Nonleaf: Unpinning parent_pagenum: "<< n->parent_pagenum << "\n";
    if((rc = pfh.UnpinPage(n->parent_pagenum)))
        return rc;

    return rc;
}


RC IX_IndexHandle::PickNext_NonLeaf(IX_Node node, IX_Node *group1[], IX_Node *group2[], PF_PageHandle group1_ph[], PF_PageHandle group2_ph[], bool used[], int &next, int &size_g1, int &size_g2){
    RC rc;

    //get area of group1
    double area_g1;
    int data_g1[4];
    double maxX1, minX1, maxY1, minY1;
    for(int i = 0; i < 3; i++){
        if(group1[i] == NULL)
            break;
        else{
            IX_Node *child;
            PageNum child_page;
            if((rc = group1_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, group1_ph[i])) || (rc = group1_ph[i].GetData((char *&) child)))
                std::cout << "DOESNT WORK\n";
            if(i == 0){
                maxX1 = child->x1; 
                minX1 = child->x2;
                maxY1 = child->y1;
                minY1 = child->y2;
            }
            else{
                maxX1 = node.max(maxX1, child->x1);
                minX1 = node.min(minX1, child->x2);
                maxY1 = node.max(maxY1, child->y1);
                minY1 = node.min(minY1, child->y2);
            }
            area_g1 = (maxX1 - minX1) * (maxY1 - minY1);
            std::cout << "PickNext nonleaf g1: Unpinning child_page: " << child_page << "\n";
            if((rc = pfh.UnpinPage(child_page)))
                return rc;
        }
    }

    //get area of group2 
    double area_g2;
    int data_g2[4];
    double maxX2, minX2, maxY2, minY2;
    for(int i = 0; i < 3; i++){
        if(group2[i] == NULL)
            break;
        else{
            IX_Node *child;
            PageNum child_page;
            if((rc = group2_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, group2_ph[i])) || (rc = group2_ph[i].GetData((char *&) child)))
                std::cout << "DOESNT WORK\n";
            if(i == 0){
                maxX2 = child->x1; 
                minX2 = child->x2;
                maxY2 = child->y1;
                minY2 = child->y2;
            }
            else{
                maxX2 = node.max(maxX2, child->x1);
                minX2 = node.min(minX2, child->x2);
                maxY2 = node.max(maxY2, child->y1);
                minY2 = node.min(minY2, child->y2);
            }
            area_g2 = (maxX2 - minX2) * (maxY2 - minY2);
            std::cout << "PickNext nonleaf g2: Unpinning child_page:" << child_page << "\n";
            if((rc = pfh.UnpinPage(child_page)))
                return rc;
        }
    }

    double d1_best_so_far = -1;
    double d2_best_so_far = -1;
    int index1 = -1;
    int index2 = -1;
    //loop through the data records
    for(int i = 0; i < 5; i++){
        if(used[i]) //skip data records already in a group
            continue;
        else{
            //get the node's children mbrs
            IX_Node *child;
            PageNum child_page;
            if((rc = node.children_ph[i].GetPageNum(child_page)) || (rc = pfh.GetThisPage(child_page, node.children_ph[i])) || (rc = node.children_ph[i].GetData((char *&) child)))
                std::cout << "DOESNT WORK\n";  

            double d1 = 0;
            double d2 = 0;
            double maxX, minX, maxY, minY;

            maxX = node.max(child->x1, maxX1);
            minX = node.min(child->x2, minX1);
            maxY = node.max(child->y1, maxY1);
            minY = node.min(child->y2, minY1);

            d1 = ((maxX - minX) * (maxY - minY)) - area_g1; 
            if(d1_best_so_far == -1){
                d1_best_so_far = d1;
                index1 = i;
            }
            else if(d1 < d1_best_so_far){
                d1_best_so_far = d1;
                index1 = i;
            }
            
            maxX = node.max(child->x1, maxX2);
            minX = node.min(child->x2, minX2);
            maxY = node.max(child->y1, maxY2);
            minY = node.min(child->y2, minY2);

            d2 = ((maxX - minX) * (maxY - minY)) - area_g2;
            if(d2_best_so_far == -1){
                d2_best_so_far = d2;
                index2 = i;
            }
            else if(d2 < d2_best_so_far){
                d2_best_so_far = d2;
                index2 = i;
            }

            std::cout << "PickNext nonleaf get children mbr: Unpinning child_page: " << child_page << "\n";
            if((rc = pfh.UnpinPage(child_page)))
                return rc;
        }
    }

    //Add next entry to group
    if(d1_best_so_far < d2_best_so_far){
        //insert at empty slot
        for(int i = 0; i < 3; i++){
            if(group1[i] == NULL){
                group1[i] = node.children[index1];
                group1_ph[i] = node.children_ph[index1];
                next = index1;
                size_g1++;
                break;
            }
        }
    }
    else{
        //insert at empty slot
        for(int i = 0; i < 3; i++){
            if(group2[i] == NULL){
                group2[i] = node.children[index2];
                group2_ph[i] = node.children_ph[index2];
                next = index2;
                size_g2++;
                break;
            }
        }
    }

    return rc;
}

IX_Node* IX_IndexHandle::FindLeaf(IX_Node *T, void* pData1, void* pData2, void* pData3, void* pData4)
{
    //If T is not a leaf, check each entry F in T to determine if F.I overlaps E.I.
    //For each such entry invoke FindLeaf on the tree whose root is pointed to by F.p
    //until E is found or all entries have been checked.
    
    int x1 = *(int*)pData1;
    int x2 = *(int*)pData3;
    int y1 = *(int*)pData2;
    int y2 = *(int*)pData4;


    int Emin_x = T->min(x1, x2);
    int Emin_y = T->min(y1, y2);
    int Emax_x = T->max(x1, x2);
    int Emax_y = T->max(y1, y2);


    if(!T->isLeaf)
    {
        for(int i = 0; i < 5; i++)
        {
            IX_Node child = *(T->children[i]);
            if(T->children[i] == NULL)
            {
                int min_x = child.min(child.x1,child.x2);
                int min_y = child.min(child.y1,child.y2);
                int max_x = child.max(child.x1,child.x2);
                int max_y = child.max(child.y1,child.y2);
            
    
                if(Emin_x >= min_x && Emin_x <= max_x)
                {
                    if((Emin_y >= min_y && Emin_y <= max_y) 
                    || (Emax_y >= min_y && Emax_y <= max_y))
                    {
                        return FindLeaf(T->children[i], pData1, pData2, pData3, pData4);
                    }
                }
                if(Emax_x >= min_x && Emax_x <= max_x)
                {
                    if((Emin_y >= min_y && Emin_y <= max_y)
                    || (Emax_y >= min_y && Emax_y <= max_y))
                    {
                        return FindLeaf(T->children[i], pData1, pData2, pData3, pData4);
                    }
                } 
            }
        }
    }
    else
    {
        int min_x = T->min(T->x1,T->x2);
        int min_y = T->min(T->y1,T->y2);
        int max_x = T->max(T->x1,T->x2);
        int max_y = T->max(T->y1,T->y2);
    
        if(min_x <=  Emin_x && min_y <= Emin_y && max_x >= Emax_x && max_y >= Emax_y)
        {
            return T;
        }
    }
    return NULL;
}

//Need recursive function to deal with subtrees in condense tree
RC IX_IndexHandle::CondenseTreeHelper_DisposeChildren(IX_Node *N, PageNum N_pageNum, std::vector<IX_Node> *Q)
{
    if(N == NULL)
    {
        return 0;
    }

  //  RM_FileHandle rfh;
  //  header.rmm->OpenFile(header.relname, rfh);
  //  RM_Record rec;
    int rc;
    for(int i = 0; i < 5; i++)
    {
        if(N->children[i] == NULL)
        {
            continue;
        }
        if(N->children[i]->isLeaf)
        {
            IX_Node duplicate;
            duplicate = *N;
            Q->push_back(duplicate);
            std::cout << "DisposeChildren: Disposing page: " << N_pageNum << "\n"; 
            if(rc = pfh.DisposePage(N_pageNum))
                return rc;
        }
        else
        {
            PageNum CN_pageNum;
            N->children_ph[i].GetPageNum(CN_pageNum);
            if(rc = CondenseTreeHelper_DisposeChildren(N->children[i], CN_pageNum, Q))
                return rc;
            
            std::cout << "DisposeChildren: Disposing page: " << N_pageNum << "\n"; 
            if(rc = pfh.DisposePage(N_pageNum))
                return rc;
        }
    }
}


RC IX_IndexHandle::CondenseTree(IX_Node *L)
{
    std::cout << "CondenseTree: Start\n";
    int rc;
    IX_Node *N = L;
    //Set Q to be set of eliminated entries stored in node objects, initially empty.
    std::vector<IX_Node> Q;
                       
    RM_FileHandle rfh;
    header.rmm->OpenFile(header.relname, rfh);
    RM_Record rec; 
  
    //If this node is a root, start reinsertion
    while(N->parent != NULL)
    {
        IX_Node * P = N->parent;
        PageNum N_pageNum; 
        int countChildren = 0;
        if(N->isLeaf)
        {
            RID blankrid;
            for(int i =0; i< 5; i++)
            {
                if(!(N->rids[i] == blankrid))
                {
                    countChildren++;
                }
            }
        }
        else
        {
            for(int i =0; i < 5; i++)
            {
                if(N->children[i] != NULL)
                {
                    countChildren++;
                }
            }
        }
        // m =2, at least 2 child needed per node
        if(countChildren < 2)
        {
            //delete N from P, make N orphaned, add N to Q
            for(int i =0; i < 5; i++)
            {
                if(P->children[i] == N)
                {
                    //delink N from P
                    P->children[i] = NULL;
                    if(rc = (P->children_ph[i].GetPageNum(N_pageNum)))
                        return rc;

                    //Restructure P node
                    for(int j = 4; j >= 0; j--)
                    {
                        if(P->children[j] != NULL)
                        {
                            P->children[i] = P->children[j];
                            P->children[j] = NULL;
                            P->children_ph[i] = P->children_ph[j];
                        }
                    }
                }
            }
            //ADD N TO Q
            //Make a copy not an alias of N and push into Q 
            //for later insertion then dispose N
            IX_Node duplicate;
            if(N->isLeaf)
            {
                duplicate = *N;
                Q.push_back(duplicate);
                std::cout << "CondenseTree: Disposing page " << N_pageNum << "\n";
                if(rc = pfh.DisposePage(N_pageNum))
                    return rc;
            }
            //If N is not a leaf, need to delete this node and N's child nodes while putting them into Q
            else
            {
                if(rc = CondenseTreeHelper_DisposeChildren(N, N_pageNum, &Q))
                    return rc;
            }

        }
        else
        {
            //CT4: Readjust MBR to reflect deletion
            if(N->isLeaf)
            {
               //recalculate area of N
                RID empty;
                int area_N;
                int data_N[4];
                int maxX, minX, maxY, minY;
                for(int i = 0; i < 5; i++)
                {
                    if(N->rids[i] == empty)
                        break;
                    else
                    {
                        if((rc = rfh.GetRec(N->rids[i], rec)))
                            return rc;
                        for(int j = 0; j < 4; j++)
                            data_N[j] = *(rec.data + (4 * j));
                        if(i == 0)
                        {
                            maxX = N->max(data_N[0], data_N[2]);
                            minX = N->min(data_N[0], data_N[2]);
                            maxY = N->max(data_N[1], data_N[3]);
                            minY = N->min(data_N[1], data_N[3]);
                            
                        }
                        else
                        {
                            maxX = N->max(maxX, data_N[0], data_N[2]);
                            minX = N->min(minX, data_N[0], data_N[2]);
                            maxY = N->max(maxY, data_N[1], data_N[3]);
                            minY = N->min(minY, data_N[1], data_N[3]);
                        }
                        area_N = (maxX - minX) * (maxY - minY);
                    }
                }
                N->area = area_N; 
            }
            else
            {
                for(int j = 0; j < 5; j++)
                {
                    if(N->children[j] != NULL)
                    {
                        CalcMBR(N, N, N->children[j]);
                    }
                }
            }

        }
        //Set N=P to go up one level in tree, start from top
        N = P;
    }
    //Reinsertion: Entries from eliminated leaf nodes are inserted back normally,
    //Entries from higher level nodes must be placed higher in the tree so that 
    //leaves of their dependant subtrees will be on the same level as leaves of the
    //main tree
    while(!Q.empty())
    {
        std::cout << "CondenseTree: reinserting\n";
        IX_Node ins = Q.back();
        int ins_data[4];
        RID empty;
        for(int k = 0; k < 5; k++)
        {
            if(ins.rids[k] == empty)
            {
                continue;
            }
            if((rc = rfh.GetRec(ins.rids[k], rec)))
                return rc;
            for(int j = 0; j < 4; j++)
                ins_data[j] = *(rec.data + (4 * j));
            InsertEntry(&(ins_data[0]),&(ins_data[1]),&(ins_data[2]),&(ins_data[3]),ins.rids[k]);
        }
        Q.pop_back();
    }
    std::cout << "CondenseTree: Ended\n";
}

