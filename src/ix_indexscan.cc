//
// File:        ix_indexscan.cc
// Description: IX_IndexHandle handles scanning through the index for a 
//              certain value.
// Author:      <Your name here>
//

#include <unistd.h>
#include <sys/types.h>
#include "pf.h"
#include "ix.h"
#include <cstdio>

/*
 * The following functions are comparison functions that return 0 if 
 * the two objects are equal, <0 if the first value is smaller, and
 * >0 if the second value is smaller.
 * They must take in an attribute type and attribute length, which 
 * determines the basis to compare the values on.
 */
bool ix_equal(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 == *(int *)value2) ;
}

bool ix_less_than(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 < *(int *)value2);
}

bool ix_greater_than(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 > *(int *)value2) ;
}

bool ix_less_than_or_eq_to(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 <= *(int *)value2) ;
}

bool ix_greater_than_or_eq_to(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 >= *(int *)value2) ;
}

bool ix_not_equal(void * value1, void * value2, AttrType attrtype, int attrLength)
{
    return (*(int *)value1 != *(int *)value2) ;
}


IX_IndexScan::IX_IndexScan()
{
    value = -1;
    openScan = false;
    scanEnded = false;
    foundLastValue = false;
    indexAttr  = 0; 
}

IX_IndexScan::~IX_IndexScan()
{
    if(scanEnded == false)
    {

        sort(pagesToUnpin.begin(), pagesToUnpin.end());
        pagesToUnpin.erase( unique(pagesToUnpin.begin(), pagesToUnpin.end()), pagesToUnpin.end());
        for(int i =0; i < pagesToUnpin.size(); i++)
        {
            
            if(pagesToUnpin.at(i) != indexHandle->header.rootPage)
            {
                std::cout << "CloseScan: Unpinning page " << pagesToUnpin.at(i)<< "\n";
                indexHandle->pfh.UnpinPage(pagesToUnpin.at(i));
                std::cout << "CloseScan: Unpinned page " << pagesToUnpin.at(i) << "\n";
            }
        }
    }
}

RC IX_IndexScan::OpenScan(const IX_IndexHandle &indexHandle,
                CompOp compOp,
                void *value,
                int indexAttr,
                ClientHint  pinHint)
{
    std::cout << "OpenScan: Opening scan where indexAttr is: " << indexAttr <<" \n";
    this->indexAttr = indexAttr;
    RC rc = 0;
    if(openScan == true)
        return (IX_INVALIDSCAN);
    if(compOp == NE_OP)
    {
        return (IX_INVALIDSCAN);
    }
    
    this->indexHandle = const_cast<IX_IndexHandle*>(&indexHandle);
    this->value =-1;
    this->compOp = compOp;

    switch(compOp)
    {
        case EQ_OP : comparator = &ix_equal; break;
        case LT_OP : comparator = &ix_less_than; break;
        case GT_OP : comparator = &ix_greater_than; break;
        case LE_OP : comparator = &ix_less_than_or_eq_to; break;
        case GE_OP : comparator = &ix_greater_than_or_eq_to; break;
        case NE_OP : comparator = &ix_not_equal; break;
        case NO_OP : comparator = NULL; break;
        default: return(IX_INVALIDSCAN);
    }

    if(compOp != NO_OP)
    {
        std::cout << "OpenScan: Setting value to " << *(int*)value << "\n"; 
        this->value = *(int*)value;
    }

    openScan = true;
    foundLastValue = false;
    firstScan = true;
    scanEnded = false;
    
    ridschecked.clear();
    to_traverse.clear();
    
    std::cout << "OpenScan: Opened scan \n";
    return rc;
}

RC IX_IndexScan::GetNextEntry(RID &rid)
{
    std::cout << "GetNextEntry: Finding attribute " << indexAttr << " of value " << value << "\n";
    RC rc = 0;
    RID temp;
    rid = temp;
    if(scanEnded == true && openScan == true)
        return (IX_EOF);
    if(foundLastValue == true)
        return (IX_EOF);

    if(scanEnded == true || openScan == false)
        return (IX_INVALIDSCAN);

    bool notFound = true;
    struct IX_Node *T;
    //std::cout << "GETTING ROOT\n";
    //if(!firstScan){
    //    std::cout << "GetNextEntry: Pinning root page\n";
    //    if((rc = indexHandle->pfh.GetThisPage(indexHandle->header.rootPage, indexHandle->root_ph)))
    //        return rc;
    //}
    //
    //

    //indexHandle->pfh.printBuffer();

    if((rc = indexHandle->root_ph.GetData((char *&)T)))
        return rc;
    PageNum TpageNum = indexHandle->header.rootPage;
    for(int k = 0; k < 5; k++)
    {
        std::cout << "\nGetNextEntry: Checking root rids: T->rids[" << k <<"] page: " << T->rids[k].page<<"\n";
    }
    
    
    //std::cout << "GetNextEntry pagesToUnPin: push_back " << indexHandle->header.rootPage << "\n";
    //this->pagesToUnpin.push_back(indexHandle->header.rootPage);
    while(notFound)
    {
        if(!firstScan)
        {
            if(!to_traverse.empty())
            {
                std::cout << "GetNextEntry: Getting next T\n";
//
  //              std::cout <<"\nn GetNextEntry: Unpinning page root page\n";
    //            if(rc = indexHandle->pfh.UnpinPage(1))
      //          {
        //            std::cout << "WhAT\n";
        //            return rc;
        //        }
         //       std::cout << "\nGetNextEntry: After unpin 1\n";

                TpageNum = to_traverse.back();
                PF_PageHandle T_ph;
                if(rc = indexHandle->pfh.GetThisPage(TpageNum, T_ph))
                    return rc;
                this->pagesToUnpin.push_back(TpageNum);
                if(rc = T_ph.GetData((char *&)T))
                    return rc;
                to_traverse.pop_back();
                if((rc = indexHandle->root_ph.GetData((char *&)T)))
                    return rc;
                PageNum TpageNum = indexHandle->header.rootPage;
                for(int k = 0; k < 5; k++)
                {
                    std::cout << "\nGetNextEntry: Checking root rids: T->rids[" << k <<"] page: " << T->rids[k].page<<"\n";
                }
            }
            else
            {
                return IX_EOF;
            }
        }
        firstScan = false;

        while(!T->isLeaf)
        {
            for(int i = 0; i < 5; i++)
            {
                //std::cout << i << "\n";
                if(T->children[i] != NULL)
                {
                    PageNum child_page;
                    PF_PageHandle childPH;
                    IX_Node * child;
                    if((rc = T->children_ph[i].GetPageNum(child_page)) 
                            || (rc = indexHandle->pfh.GetThisPage(child_page, T->children_ph[i])) 
                            || (rc = T->children_ph[i].GetData((char *&) child)))
                        std::cout << "DOESNT WORK\n";
                    if(indexAttr == 0 || indexAttr == 2)
                    {
                        if(compOp == EQ_OP && (child->x1 >= value && child->x2 <= value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
     
                        else if(compOp == LT_OP && (child->x2 < value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == GT_OP && (child->x1 > value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == LE_OP && child->x2 <= value)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == GE_OP && child->x1 >= value)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == NE_OP && (child->x1 < value && child->x2 > value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                        else if(compOp == NO_OP)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                        else
                        {
                            std::cout << "GetNextEntry: Unpinning page " << child_page << "\n";
                            if(rc = indexHandle->pfh.UnpinPage(child_page))
                                return rc;
                        }
                    }
                    else
                    {
                        std::cout << "GetNextEntry: Setting up for finding y values\n";
                        if(compOp == EQ_OP && (child->y1 >= value && child->y2 <= value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
     
                        else if(compOp == LT_OP && (child->y2 < value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == GT_OP && (child->y1 > value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == LE_OP && child->y2 <= value)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == GE_OP && child->y1 >= value)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                       
                        else if(compOp == NE_OP && (child->y1 < value && child->y2 > value))
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                        else if(compOp == NO_OP)
                        {
                            to_traverse.push_back(child_page);
                            pagesToUnpin.push_back(child_page);
                        }
                        else
                        {
                            std::cout << "GetNextEntry: Unpinning page " << child_page << "\n";
                            if(rc = indexHandle->pfh.UnpinPage(child_page))
                                return rc;
                        }
                    }
                }
            }
            if(!to_traverse.empty())
            {
                TpageNum = to_traverse.back();
                PF_PageHandle T_ph;
                if(rc = indexHandle->pfh.GetThisPage(TpageNum, T_ph))
                    return rc;
                this->pagesToUnpin.push_back(TpageNum);
                if(rc = T_ph.GetData((char *&)T))
                    return rc;
                to_traverse.pop_back();
            }
            else
                return (IX_EOF);
        }
        RID empty;
        RM_FileHandle rfh;
        indexHandle->header.rmm->OpenFile(indexHandle->header.relname, rfh);
        RM_Record rec;
        for(int i = 0; i < 5; i++)
        {
            std::cout << "GetNextEntry: Going through rid entries \n";
            if(!(T->rids[i] == empty))
            {
                if(compOp == NO_OP)
                {
                    std::vector<int>::iterator it;
                    it = find(ridschecked.begin(), ridschecked.end(),i);
                    if(it != ridschecked.end())
                    {
                        continue;
                    }
                    
                    rid = T->rids[i];
                    notFound=false;
                    if(i != 4)
                    {
                        ridschecked.push_back(i);
                        to_traverse.push_back(TpageNum);
                        break;
                    }
                    else
                    {
                        ridschecked.clear();
                        break;
                    }

                }
                else
                {
                    std::vector<int>::iterator it;
                    it = find(ridschecked.begin(), ridschecked.end(),i);
                    if(it != ridschecked.end())
                    {
                        continue;
                    }
                    std::cout << "GetNextEntry: rids[" << i << "] has pagenum " << T->rids[i].page << " slot " << T->rids[i].slot << " \n";

                    if(rc = rfh.GetRec(T->rids[i], rec))
                        return rc;
                    int d[4];
                    std::cout << "GetNextEntry: getting data from rid "; 
                    for(int j = 0; j< 4; j++)
                    {
                        d[j] = *(rec.data+(j*4));
                        std::cout << d[j] << " ";
                    }

                    std::cout << "\n";
                    int check = d[indexAttr];
                    if((comparator((void*)&check,(void*)&value, INT, 4)))
                    {

                        rid = T->rids[i];
                        notFound=false;
                        if(i != 4)
                        {
                            ridschecked.push_back(i);
                            to_traverse.push_back(TpageNum);
                            break;
                        }
                        else
                        {
                            ridschecked.clear();
                            break;
                        }
                    }
                }
            }
            else
            {
                ridschecked.clear();
                break;
            }
        }
    }
    
    //indexHandle->pfh.UnpinPage(1);



    return rc;
}

RC IX_IndexScan::CloseScan()
{
    std::cout << "CloseScan: Closing scan \n";
    RC rc = 0;
    if(openScan == false)
    {
        std::cout << "CloseScan: OpenScan was false\n";
        return (IX_INVALIDSCAN);
    }
    if(scanEnded == false)
    {
    }

    sort(pagesToUnpin.begin(), pagesToUnpin.end());
    pagesToUnpin.erase( unique(pagesToUnpin.begin(), pagesToUnpin.end()), pagesToUnpin.end());
    for(int i =0; i < pagesToUnpin.size(); i++)
    {
        
    //    if(pagesToUnpin.at(i) != indexHandle->header.rootPage)
    //    {
            std::cout << "CloseScan: Unpinning page " << pagesToUnpin.at(i)<< "\n";
            if( (rc = indexHandle->pfh.UnpinPage(pagesToUnpin.at(i))))
                return rc;
            std::cout << "CloseScan: Unpinned page " << pagesToUnpin.at(i) << "\n";
    //    }
    }
    pagesToUnpin.clear();
    openScan=false;
    
    return rc;
}
