//
// ix.h
//
//   Index Manager Component Interface
//

#ifndef IX_H
#define IX_H

#include "redbase.h"  // Please don't change these lines
#include "rm_rid.h"  // Please don't change these lines
#include "pf.h"
#include <string>
#include <cstdlib>
#include <cstring>
#include "rm.h"
#include <iostream>
#include <vector>
#include <algorithm>
struct IX_IndexHeader{
    const char *file;
    int min_entries;
    int max_entries;
    PageNum rootPage;
    AttrType attrType1;
    AttrType attrType2;
    AttrType attrType3;
    AttrType attrType4;
    int attrLength1;
    int attrLength2;
    int attrLength3;
    int attrLength4;
    int totalEntries;
    const char *relname;
    RM_Manager *rmm;
};

struct IX_Node{
    PageNum myPageNum;
    bool isLeaf;
    bool isEmpty;
    bool isFull;
    double area;
    double x1;
    double x2;
    double y1;
    double y2;
    IX_Node *children[5];
    PF_PageHandle children_ph[5];
    IX_Node *parent;
    PageNum parent_pagenum;
    RID rids[5]; 
    IX_Node(){
        myPageNum = -1;
        isLeaf = true;
        isEmpty = true;
        isFull = false;
        area = 0;
        x1 = 0;
        x2 = 0;
        y1 = 0;
        y2 = 0;
        parent = NULL;
        parent_pagenum = -1;
        RID temp;
        for(int i = 0; i < 5; i++){
            children[i] = NULL;
            rids[i] = temp;
        }
    }
    IX_Node& operator = (const IX_Node &rhs){
        isLeaf = rhs.isLeaf;
        isEmpty = rhs.isEmpty;
        isFull = rhs.isFull;
        area = rhs.area;
        x1 = rhs.x1;
        x2 = rhs.x2;
        y1 = rhs.y1;
        y2 = rhs.y2;
        parent = rhs.parent;
        parent_pagenum = rhs.parent_pagenum;
        for(int i = 0; i < 5; i++)
            children[i] = rhs.children[i];
        for(int i = 0; i < 5; i++)
            rids[i] = rhs.rids[i];
    }
    double max(double a, double b){
        if(a >= b)
            return a;
        else return b;
    }
    double max(double a, double b, double c){
        if(a >= b && a >= c)
            return a;
        else if(b >= a && b >= c)
            return b;
        else
            return c;
    }
    double min(double a, double b){
        if(a <= b)
            return a;
        else 
            return b;
    }
    double min(double a, double b, double c){
        if(a <= b && a <= c)
            return a;
        else if(b <= a && b <= c)
            return b;
        else
            return c;
    }
    /*RC GetRec (const RID &rid, RM_Record &rec, PF_FileHandle pfh) const {
      // Retrieves page and slot number from RID
      RC rc = 0;
      PageNum page;
      SlotNum slot;
      std::cout << "***IN GetRec***\n"; 
      //GetPageNumAndSlot procedure
      if((rc = rid.isValidRID()))
        return (rc);  
      std::cout << "OK1\n";
      if((rc = rid.GetPageNum(page)))
        return (rc);
      std::cout << "OK2\n";
      if((rc = rid.GetSlotNum(slot)))
        return (rc);
      //return (0);
        std::cout << "Page = " << page << ". Slot = " << slot << "\n";
      // Retrieves the appropriate page, and its bitmap and pageheader 
      // contents
      PF_PageHandle ph;
      if((rc = pfh.GetThisPage(page, ph))){
        return (rc);
      }
      std::cout << "OK3\n";
      char *bitmap;
      struct RM_PageHeader *pageheader;
      //GetPageDataAndBitmap procedure
      char * pData;
      if((rc = ph.GetData(pData)))
        return (rc);
      std::cout << "OK4\n";
      pageheader = (struct RM_PageHeader *) pData;

      RM_FileHeader *header = (struct RM_FileHeader *) pData;

      bitmap = pData + header->bitmapOffset; // retrieve bitmap pointer
      // Check if there really exists a record here according to the header
      bool recordExists;
      std::cout << "OK5\n"; 
      //CheckBitSet procedure
      if(slot > header->numRecordsPerPage)
        return (RM_INVALIDBITOPERATION);
      int chunk = slot / 8;
      int offset = slot - chunk*8;
      for(int i = 0; i < slot; i++){
        if(bitmap[i] == 0)
            std::cout << "0\n";
        else
            std::cout << "1\n";
      }
      std::cout << bitmap << "\n";
      *if ((bitmap[chunk] & (1 << offset)) != 0)
        recordExists = true;
      else
        recordExists = false;      
      
      if(!recordExists){
        rc = RM_INVALIDRECORD;
        goto cleanup_and_exit;
      }*
      std::cout << "OK6\n";
      // Set the record and return it
      if((rc = rec.SetRecord(rid, bitmap + (header->bitmapSize) + slot*(header->recordSize), 
        header->recordSize)))
        goto cleanup_and_exit;
      std::cout << "DONE\n";
      // always unpin the page before returning
      cleanup_and_exit:
      RC rc2;
      if((rc2 = pfh.UnpinPage(page)))
        return (rc2);
      return (rc); 
    }*/
};

//
// IX_IndexHandle: IX Index File interface
//
class IX_IndexHandle {
private:
    IX_Node * ChooseLeaf(void *pData1, void *pData2, void *pData3, void *pData4, IX_Node * root, PageNum &CL_pagenum);
    double CalcArea(void *pData1, void *pData2, void *pData3, void *pData4, IX_Node * root);
    RC SplitNode(IX_Node *node, IX_Node *new_node);
    RC PickSeeds(IX_Node *node, int &e1, int &e2);
    RC PickNext(IX_Node node, RID group1[], RID group2[], bool used[], int &next, int &size_g1, int &size_g2);
    RC PickNext_NonLeaf(IX_Node node, IX_Node *group1[], IX_Node *group2[], PF_PageHandle group1_ph[], PF_PageHandle group2_ph[], bool used[], int &next, int &size_g1, int &size_g2);
    RC AdjustTree(IX_Node *node, IX_Node *new_node, PF_PageHandle newPH, bool leaf_split_occurred);
    void CalcMBR(IX_Node *dest, IX_Node *src1, IX_Node *src2);
    RC SplitNode_NonLeaf(IX_Node *n, IX_Node *nn, PF_PageHandle &nnPH, bool &propogate);
    IX_Node* FindLeaf(IX_Node *T, void* pData1, void* pData2, void* pData3, void* pData4);
    
    RC CondenseTreeHelper_DisposeChildren(IX_Node *N, PageNum N_pageNum, std::vector<IX_Node> *Q);
    RC CondenseTree(IX_Node* L);
    
public:
    IX_IndexHandle();
    ~IX_IndexHandle();

    // Insert a new index entry
    RC InsertEntry(void *pData1, void *pData2, void *pData3, void *pData4, const RID &rid);

    // Delete a new index entry
    RC DeleteEntry(void *pData1, void *pData2, void *pData3, void *pData4, const RID &rid);

    // Force index files to disk
    RC ForcePages();

    PF_PageHandle root_ph;
    bool header_modified;
    bool openedIXH;
    struct IX_IndexHeader header;
    PF_FileHandle pfh;
};

//
// IX_IndexScan: condition-based scan of index entries
//
class IX_IndexScan {
public:
    IX_IndexScan();
    ~IX_IndexScan();

    // Open index scan
    RC OpenScan(const IX_IndexHandle &indexHandle,
                CompOp compOp,
                void *value,
                int indexAttr,
                ClientHint  pinHint = NO_HINT);


    // Get the next matching entry return IX_EOF if no more matching
    // entries.
    RC GetNextEntry(RID &rid);

    // Close index scan
    RC CloseScan();

    int value;
    CompOp compOp;
    IX_IndexHandle *indexHandle;
    bool (*comparator)(void *, void*, AttrType, int);
    RID current_rid;
    RID next_rid;
   
    IX_Node * node;

    bool scanEnded;
    bool openScan;
    bool foundLastValue;
    bool firstScan;
    std::vector<PageNum> pagesToUnpin;
    std::vector<PageNum> to_traverse;    
    std::vector<int> ridschecked;
    int indexAttr;
};

//
// IX_Manager: provides IX index file management
//
class IX_Manager {
private:
    PF_Manager &pf_man;
public:
    IX_Manager(PF_Manager &pfm);
    ~IX_Manager();

    // Create a new Index
    RC CreateIndex(const char *fileName, int indexNo,
                   AttrType attrType1, int attrLength1,
                   AttrType attrType2, int attrLength2,
                   AttrType attrType3, int attrLength3,
                   AttrType attrType4, int attrLength4,
                   RM_Manager &rmm);

    // Destroy and Index
    RC DestroyIndex(const char *fileName, int indexNo1, int indexNo2, int indexNo3, int indexNo4);

    // Open an Index
    RC OpenIndex(const char *fileName, int indexNo,
                 IX_IndexHandle &indexHandle);

    // Close an Index
    RC CloseIndex(IX_IndexHandle &indexHandle);
};

//
// Print-error function
//
void IX_PrintError(RC rc);

#define IX_BADINDEXSPEC         (START_IX_WARN + 0) // Bad Specification for Index File
#define IX_BADINDEXNAME         (START_IX_WARN + 1) // Bad index name
#define IX_INVALIDINDEXHANDLE   (START_IX_WARN + 2) // FileHandle used is invalid
#define IX_INVALIDINDEXFILE     (START_IX_WARN + 3) // Bad index file
#define IX_NODEFULL             (START_IX_WARN + 4) // A node in the file is full
#define IX_BADFILENAME          (START_IX_WARN + 5) // Bad file name
#define IX_INVALIDBUCKET        (START_IX_WARN + 6) // Bucket trying to access is invalid
#define IX_DUPLICATEENTRY       (START_IX_WARN + 7) // Trying to enter a duplicate entry
#define IX_INVALIDSCAN          (START_IX_WARN + 8) // Invalid IX_Indexscsan
#define IX_INVALIDENTRY         (START_IX_WARN + 9) // Entry not in the index
#define IX_EOF                  (START_IX_WARN + 10)// End of index file
#define IX_LASTWARN             IX_EOF

#define IX_ERROR                (START_IX_ERR - 0) // error
#define IX_LASTERROR            IX_ERROR

#endif
