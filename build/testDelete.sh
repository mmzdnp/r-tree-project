!#/usr/bin/bash

make
./dbdestroy Test
./dbcreate Test
rm ./Test/test
rm ./Test/test.0
./redbase Test <<< "create table test(x1 i, y1 i, x2 i, y2 i);"
./redbase Test <<< "insert into test values(1,0,5,1);"
./redbase Test <<< "create index test(x1, y1, x2, y2);"
./redbase Test <<< "insert into test values(1,1,5,2);"
./redbase Test <<< "insert into test values(1,2,5,3);"
./redbase Test <<< "insert into test values(1,3,5,4);"
./redbase Test <<< "insert into test values(1,4,5,5);"
./redbase Test <<< "delete from test where y1=2;"
