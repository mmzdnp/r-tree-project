make
./dbdestroy Test
./dbcreate Test
rm ./Test/test
rm ./Test/test.0
./redbase Test <<< "create table test(x1 i, y1 i, x2 i, y2 i);"
./redbase Test <<< "insert into test values(0,0,1,1);"
./redbase Test <<< "create index test(x1, y1, x2, y2);"
./redbase Test <<< "insert into test values(0,2,1,3);"
./redbase Test <<< "insert into test values(2,2,3,3);"
./redbase Test <<< "insert into test values(2,0,3,1);"
./redbase Test <<< "insert into test values(1,1,2,2);"
#1st split at next insert
./redbase Test <<< "insert into test values(4,2,5,3);"
./redbase Test <<< "insert into test values(4,0,5,1);"
./redbase Test <<< "insert into test values(5,1,6,2);"
./redbase Test <<< "insert into test values(3,3,4,4);"
./redbase Test <<< "insert into test values(6,0,7,1);"
./redbase Test <<< "insert into test values(7,1,8,2);"
./redbase Test <<< "insert into test values(1,3,2,4);"

